{exp:stash:set type="snippet" parse_vars="yes" parse_tags="yes" parse_conditionals="yes"}
	
	{stash:page_color_context}grey_page{/stash:page_color_context}
	
	{stash:page_title}My Account Dashboard{/stash:page_title}

	{stash:main_content}
		
		{if logged_out}
			<p>You must log in to use this feature.</p>
			{sn.backstage.login_form}
		{/if}
		
		{if logged_in}
			
		{exp:switchee parse="inward" variable="{segment_3}"}

			{case value="success"}
			
				<p class="breadcrumb"><a href="/">Home</a> &rarr; <a href="/backstage">My Account</a> &rarr; Edit Directory Listings</p>
				
				<h2>Success!</h2>
				
				<p class="alert alert-success">Directory updated.</p>
				
				<p><a href="/backstage" class="btn arrow">Return to Dashboard</a></p>
				
			{/case}

			{case value="new|"}
				
				{exp:safecracker channel="directory" return="backstage/edit_directory_listing/success" include_jquery="no" class="form-horizontal"}
				
				<p class="breadcrumb"><a href="/">Home</a> &rarr; <a href="/backstage">My Account</a> &rarr; New Directory Listing</p>
				
				<h2>Add New Directory Listing</h2>
				
				{sn.backstage.edit_directory_listing_form}
				
				{/exp:safecracker}
				
			{/case}
		
			{case default="yes"}
			
				{exp:safecracker channel="directory" return="backstage/edit_directory_listing/success" include_jquery="no" class="form-horizontal" entry_id="{segment_3}"}
			
				<p class="breadcrumb"><a href="/">Home</a> &rarr; <a href="/backstage">My Account</a> &rarr; Edit Directory Listing &rarr; {title}</p>
			
				<h2>Edit directory listing</h2>
				
				{sn.backstage.edit_directory_listing_form}
				
				{/exp:safecracker}
			
			{/case}
		
		{/exp:switchee}
		
		{/if}

	{/stash:main_content}
	
	{stash:sidebar_content}
		
		{sn.backstage.sidebar}
	
	{/stash:sidebar_content}

{/exp:stash:set}

{embed="pages/.story_page_scaffold"}