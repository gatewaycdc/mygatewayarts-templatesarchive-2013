{exp:stash:set type="snippet" parse_vars="yes" parse_tags="yes" parse_conditionals="yes"}
	
	{stash:page_color_context}grey_page{/stash:page_color_context}
	
	{stash:page_title}My Account Dashboard{/stash:page_title}

	{stash:main_content}
		
		{if logged_out}
			<p>You must log in to use this feature.</p>
			{sn.backstage.login_form}
		{/if}
		
		{if logged_in}
			
		<p class="breadcrumb"><a href="/">Home</a> &rarr; <a href="/my/account">My Account</a> &rarr; Change Password</p>
			
		{exp:switchee parse="inward" variable="{uri_string}"}

			{case value="backstage/change_password/success"}
				
				<h2>Success!</h2>
				
				<p class="alert alert-success">Password updated.</p>
				
				<p><a class="btn arrow">Return to Dashboard</a></p>
				
			{/case}
			
			{case value="backstage/change_password"}
				
				<h2>Change Password</h2>
				
				{exp:zoo_visitor:update_form return="backstage/change_password/success"}
					
					<p>
					<label for="current_password" class="form-label">Current password:</label>
					<input type="text" name="current_password" id="current_password" class="form-text"  />
					</p>
					
					<p>
					<label for="new_password" class="form-label">New password</label>
					<input type="password" name="new_password" id="new_password" class="form-text"  />
					</p>
					
					<p>
					<label for="new_password_confirm" class="form-label">Confirm New password</label>
					<input type="password" name="new_password_confirm" id="new_password_confirm" class="form-text"  />
					</p>
					
					<p>
					<input type="submit" value="Submit" class="btn" />
					</p>
					
				{/exp:zoo_visitor:update_form}
					
				{/case}
		
			{case value="" default="yes"}
			
				{redirect=404}
			
			{/case}
		
		{/exp:switchee}
		
		{/if}

	{/stash:main_content}
	
	{stash:sidebar_content}
		
		{sn.backstage.sidebar}
	
	{/stash:sidebar_content}

{/exp:stash:set}

{embed="pages/.story_page_scaffold"}