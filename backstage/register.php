{exp:stash:set type="snippet" parse_vars="yes" parse_tags="yes" parse_conditionals="yes"}
	
	{stash:page_color_context}grey_page{/stash:page_color_context}
	
	{stash:page_title}My Account Dashboard{/stash:page_title}

	{stash:main_content}
		
		{if logged_in}
			
			<p>You're already logged in as <strong>{screen_name}</strong>.</p>
			<p>You can change your profile details and edit directory listings via your <a href="/backstage/dashboard">Dashboard</a>.</p>
			
		{/if}
		
		{if logged_out}
			
		<p class="breadcrumb"><a href="/">Home</a> &rarr; Register New Account</p>
			
		{exp:switchee parse="inward" variable="{uri_string}"}

			{case value="backstage/register/success"}
				
				<h2>Almost done!</h2>
				
				<p class="alert alert-success">We're sending you a confirmation email. Please follow the link in that email to complete your registration.</p>
				
			{/case}
			
			{case value="backstage/register"}		
				
					<p>Registering an account on the MyGatewayArts website enables you to:
					<ul>
					<li>Add and manage artist/business listings</li>
					<li>List studio space available for lease</li>
					<li>Submit events to the calendar</li>
					</ul>
					</p>
					
					<p>(Already registered? <a href="/backstage">Log in</a> now!)</p>
				
				<h2>Register New Account</h2>
					
					{exp:zoo_visitor:registration_form return='backstage/register/success'}
					
					<div class="control-group">
						<label class="control-label" for="members-name">Your Full Name</label>
						<div class="controls">
							<input type="text" name="members-name" id="members-name" class="input-xlarge">
						</div>
					</div>
					
					<div class="control-group">
						<label class="control-label" for="username">Email</label>
						<div class="controls">
							<input type="text" name="username" id="username" class="input-xlarge">
						</div>
					</div>
					
					<div class="control-group">
						<label class="control-label" for="password">Password</label>
						<div class="controls">
							<input type="password" name="password" id="password" class="input-xlarge">
						</div>
					</div>
					
					<div class="control-group">
						<label class="control-label" for="password_confirm">Password</label>
						<div class="controls">
							<input type="password" name="password_confirm" id="password_confirm" class="input-xlarge">
						</div>
					</div>
					
					<div class="control-group">
						<div class="controls">
							<input name="submit" type="submit" value="Submit Registration" />
						</div>
					</div>
					
					{/exp:zoo_visitor:registration_form}
					
				{/case}
		
			{case value="" default="yes"}
			
				{redirect=404}
			
			{/case}
		
		{/exp:switchee}
		
		{/if}

	{/stash:main_content}
	
	{stash:sidebar_content}
		
		{sn.backstage.sidebar}
	
	{/stash:sidebar_content}

{/exp:stash:set}

{embed="pages/.story_page_scaffold"}