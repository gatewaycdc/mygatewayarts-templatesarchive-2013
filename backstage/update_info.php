{exp:stash:set type="snippet" parse_vars="yes" parse_tags="yes" parse_conditionals="yes"}
	
	{stash:page_color_context}grey_page{/stash:page_color_context}
	
	{stash:page_title}My Account Dashboard{/stash:page_title}

	{stash:main_content}
		
		{if logged_out}
			<p>You must log in to use this feature.</p>
			{sn.backstage.login_form}
		{/if}
		
		{if logged_in}
			
		<p class="breadcrumb"><a href="/">Home</a> &rarr; <a href="/backstage">My Account</a> &rarr; Update Profile</p>
			
		{exp:switchee parse="inward" variable="{uri_string}"}

			{case value="backstage/update_info/success"}
				
				<h2>Success!</h2>
				
				<p class="alert alert-success">Profile updated.</p>
				
				<p><a class="btn arrow" href="/backstage">Return to Dashboard</a></p>
				
			{/case}
			
			{case value="backstage/update_info"}
				
				<h2>Update Profile</h2>
				
				{exp:zoo_visitor:update_form return="backstage/update_info/success" require_password="off"}
				<p><b>Sorry!</b> This page does not work correctly. <b>Please return to the previous page.</b><br><br><br>
		{!--		<p>
				<label for="members-name">Name</label>
				{field:members-name}
				</p>
				
				<p>
				<label for="members-phone">Phone</label>
				{field:members-phone}
				</p>
				
				<p>
				<label for="email">Email</label>
				<input id="email" name="email" type="text" value="{email}" />
				</p>
				
				<p>
				<label for="members-zip">Home ZIP</label>
				{field:members-zip}
				</p>

				
				<p>
				<label for="members-birthday">Birthday</label>
				{field:members-birthday}
				</p>
					
				
				<p>
				<input type="hidden" name="title" value="{username}">
				<input type="submit" value="Submit" class="button"/>
				</p>
					--}
				{/exp:zoo_visitor:update_form}
					
			{/case}
		
			{case value="" default="yes"}
			
				{redirect=404}
			
			{/case}
		
		{/exp:switchee}
		
		{/if}

	{/stash:main_content}
	
	{stash:sidebar_content}
		
		{sn.backstage.sidebar}
	
	{/stash:sidebar_content}

{/exp:stash:set}

{embed="pages/.story_page_scaffold"}