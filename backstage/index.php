<!-- template debug note - begin template in /backstage/index - {current_time format="%Y-%m-%d %H:%i:%s"} - member {member_id} -->

{exp:stash:set type="snippet" parse_vars="yes" parse_tags="yes" parse_conditionals="yes" refresh="0"}
	
	{stash:page_color_context}grey_page{/stash:page_color_context}
	
	{stash:page_title}My Account Dashboard{/stash:page_title}

	{stash:main_content}
		
		<!-- template debug note - begin main_content stash in /backstage/index - {current_time format="%Y-%m-%d %H:%i:%s"} - member {member_id} -->
		
		{if logged_out}
			<p><b>You must log in to use this feature. Please log in below.</b></p>
			{sn.backstage.login_form}
		{/if}
		
		{if logged_in}

		<p class="breadcrumb"><a href="/">Home</a> &rarr; <a href="/backstage">My Account</a> &rarr; Dashboard</p>
		
		{exp:switchee parse="inward" variable="{uri_string}"}

			{case value="backstage"}
				<h2>Dashboard</h2>
				<b>Welcome to the MyGatewayArts.org User Dashboard</b>. MyGatewayArts.org is a FREE online portal for promoting and learning about arts-related activities in the Gateway Arts District. <b>Use our Dashboard Services below, as well as our <a href="http://mygatewayarts.org/social-media" target="_blank">Social Media Tools</a> to find fellow artists, businesses, and residents.</b> 
<br>
<br><img src="http://mygatewayarts.org/media/site_media/blueArrow2.png" style="border-width: 0px; border-style: solid;" />  Click "+Add" below each section to <b>create calendar events, directory listings and studio listings</b>. To delete entries, or ask any questions, please e-mail our <a href="mailto:gatewayartsdistrict@mygatewayarts.org">Content Management Team</a>.
<br><br>			<h2>My Account Info</h2>	
				<table class="table table-bordered">
				<tbody>
				<!-- template debug note - before member info channe:entries in /backstage/index - {current_time format="%Y-%m-%d %H:%i:%s"} - member {member_id} -->
				{exp:channel:entries channel="members" dynamic="off" status="not closed" limit="1" author_id="CURRENT_USER" disable="{sn.disable_most}"}
				<!-- template debug note - inside member info channe:entries in /backstage/index - {current_time format="%Y-%m-%d %H:%i:%s"} - member {member_id} -->
						<tr><td>Name</td><td>{members-name}</td></tr>
						<tr><td>Email</td><td>{email}</td></tr>
<tr><td>Change Password</td><td>Click <a href="/backstage/change_password">here</a></td></tr>
						{if no_results}<p>You haven't added a personal profile yet. Please <a href="/backstage/update_info">update your information</a> soon!</p>{/if}
				{/exp:channel:entries}
				</tbody>
				</table><br>

				<table border="0" cellpadding="10"><tr><td><h2>My Events</h2></td><td><img src="http://mygatewayarts.org/media/site_media/redArrow.png" style="border-width: 0px; border-style: solid;" /> View <a href="http://mygatewayarts.org/calendar" target="_blank">Events Calendar</a> (in new window)</td></tr></table>
				
				<table class="table">
				<thead>
				<tr>
					<th>Title</th>
					<th>Start Date</th>
					<th>Edit</th>
				</tr>
				</thead>
				<tbody>
				{exp:calendar:events author_id="CURRENT_USER" dynamic="no" disable="{sn.disable_most}"}
				<!-- template debug note - inside events channe:entries in /backstage/index - {current_time format="%Y-%m-%d %H:%i:%s"} - member {member_id} -->
					<tr>
						<td>{title}</td>
						<td>{event_start_date format="%F %j, %Y"}</td>
						<td><a class="btn" href="/backstage/edit_event/{entry_id}">edit</a></td>
					</tr>
				{/exp:calendar:events}
					<tr>
						<td><em>(new Event)</em></td>
						<td></td>
						<td><a class="btn btn-success" href="/backstage/edit_event/new">+ add</a></td>
					
				</tbody>
				</table><br>
				<table border="0" cellpadding="10"><tr><td><h2>My Directory Listings</h2></td><td><img src="http://mygatewayarts.org/media/site_media/greenArrow.png" style="border-width: 0px; border-style: solid;" /> View <a href="http://mygatewayarts.org/businesses" target="_blank">Business Directory</a> (in new window)<br>
<img src="http://mygatewayarts.org/media/site_media/greenArrow.png" style="border-width: 0px; border-style: solid;" /> View <a href="http://mygatewayarts.org/artists" target="_blank">Artists Directory</a> (in new window)
</td></tr></table>
		
				
				<table class="table">
				<thead>
				<tr>
					<th>Title</th>
					<th>Type</th>
					<th>Edit</th>
				</tr>
				</thead>
				<tbody>
				{exp:channel:entries channel="directory" author_id="CURRENT_USER" dynamic="off" disable="sn.disable_most"}
					<tr>
						<td width="50%">{title}</td>
						<td>{categories show_group="5" backspace="3"}{category_name} / {/categories}</td>
						<td><a class="btn" href="/backstage/edit_directory_listing/{entry_id}">edit</a></td>
					</tr>
				{/exp:channel:entries}
					<tr>
						<td><em>(new Directory Listing)</em></td>
						<td></td>
						<td><a class="btn btn-success" href="/backstage/edit_directory_listing/new">+ add</a></td>
					</tr>
				</tbody>
				</table>

				<br>
				<table border="0" cellpadding="10"><tr><td><h2>My Studio-for-Lease Listings</h2></td><td><img src="http://mygatewayarts.org/media/site_media/greenArrow.png" style="border-width: 0px; border-style: solid;" /> View <a href="http://mygatewayarts.org/studios" target="_blank">Studio Locator</a> (in new window)</td></tr></table>
		
				<table class="table">
				<thead>
				<tr>
					<th>Title</th>
					<th>Type</th>
					<th>Edit</th>
				</tr>
				</thead>
				<tbody>
				{exp:channel:entries channel="studio_listings" author_id="CURRENT_USER" dynamic="off" disable="sn.disable_most"}
					<tr>
						<td width="50%">{title}</td>
						<td></td>
						<td><a class="btn" href="/backstage/edit_studio_listing/{entry_id}">edit</a></td>
					</tr>
				{/exp:channel:entries}
					<tr>
						<td><em>(new Studio Listing)</em></td>
						<td></td>
						<td><a class="btn btn-success" href="/backstage/edit_studio_listing/new">+ add</a></td>
					</tr>				
				</tbody>
				</table>
				
			{/case}

			{case value="backstage/dashboard"}
			
				{redirect="backstage"}
			
			{/case}
		
			{case value="" default="yes"}
			
				<h2>Option Not Found</h2>
			
			{/case}
		
		{/exp:switchee}
		
		{/if}
	{/stash:main_content}
	
	{stash:sidebar_content}
		
		{sn.backstage.sidebar}
	
	{/stash:sidebar_content}

{/exp:stash:set}

{embed="pages/.story_page_scaffold"}