{exp:calendar:events channel="calendar_events" event_name="{segment_3}"}

<p class="breadcrumb"><a href="/">Home</a> &rarr; <a href="/calendar">Calendar</a> &rarr; {title}</p>

<h1>{title}</h1>

{if events-summary}
<div>{events-summary}</div>
{/if}

{if event_has_rules}
<!--
{rules}

rule_type: {rule_type}
rule_repeat_years: {rule_repeat_years}
rule_repeat_months: {rule_repeat_months}
rule_repeat_weeks: {rule_repeat_weeks}
rule_repeat_days: {rule_repeat_days}
rule_days_of_week: {rule_days_of_week backspace="2"}{day_of_week}, {/rule_days_of_week} 
rule_relative_dow: {rule_relative_dow backspace="2"}{relative_dow}, {/rule_relative_dow} 
rule_days_of_month: {rule_days_of_month backspace="2"}{day_of_month}, {/rule_days_of_month} 
rule_months_of_year: {rule_months_of_year backspace="2"}{months_of_year}, {/rule_months_of_year} 
rule_stop_by: {rule_stop_by format="%F %j, %Y"} 
rule_stop_after: {rule_stop_after format="%F %j, %Y"} 
rule_start_date: {rule_start_date format="%F %j, %Y"} 
rule_end_date: {rule_end_date format="%F %j, %Y"}

{/rules}
-->
{/if}

<table class="table" style="margin-top:24px;">

{exp:switchee parse="inward" variable="{segment_4}"}

	{case value="|next"}
	{exp:calendar:occurrences event_id="{event_id}" prior_occurrences_limit="0" occurrences_limit="1"}
	<tr>
		<th>Date</th>
		<td>
			<p>{occurrence_start_date format="%F %j, %Y"} 
			{if occurrence_all_day != 1} &nbsp;&bull;&nbsp; {occurrence_start_date format="%g:%i %a"}{/if}
			{if occurrence_all_day != 1 OR occurrence_multi_day == 1} &ndash; {/if}
			{if occurrence_multi_day}{occurrence_end_date format="%F %j, %Y"} {/if}
			{if occurrence_all_day == 1 AND occurrence_multi_day == 1} &nbsp;&bull;&nbsp; {/if}
			{if occurrence_all_day != 1}{occurrence_end_date format="%g:%i %a"}{/if}
			</p>
		</td>
	</tr>
	{/exp:calendar:occurrences}
	{/case}
	
	{case value="all"}
	<tr>
		<th>Dates</th>
		<td>
		{exp:calendar:occurrences event_id="{event_id}"}
			<p>{occurrence_start_date format="%F %j, %Y"} 
			{if occurrence_all_day != 1} &nbsp;&bull;&nbsp; {occurrence_start_date format="%g:%i %a"}{/if}
			{if occurrence_all_day != 1 OR occurrence_multi_day == 1} &ndash; {/if}
			{if occurrence_multi_day}{occurrence_end_date format="%F %j, %Y"} {/if}
			{if occurrence_all_day == 1 AND occurrence_multi_day == 1} &nbsp;&bull;&nbsp; {/if}
			{if occurrence_all_day != 1}{occurrence_end_date format="%g:%i %a"}{/if}
			</p>
		{/exp:calendar:occurrences}
		</td>
	</tr>
	{/case}

	{case value="upcoming"}
	<tr>
		<th>Dates</th>
		<td>
		{exp:calendar:occurrences event_id="{event_id}" prior_occurrences_limit="0"}
			<p>{occurrence_start_date format="%F %j, %Y"} 
			{if occurrence_all_day != 1} &nbsp;&bull;&nbsp; {occurrence_start_date format="%g:%i %a"}{/if}
			{if occurrence_all_day != 1 OR occurrence_multi_day == 1} &ndash; {/if}
			{if occurrence_multi_day}{occurrence_end_date format="%F %j, %Y"} {/if}
			{if occurrence_all_day == 1 AND occurrence_multi_day == 1} &nbsp;&bull;&nbsp; {/if}
			{if occurrence_all_day != 1}{occurrence_end_date format="%g:%i %a"}{/if}
			</p>
		{/exp:calendar:occurrences}
		</td>
	</tr>
	{/case}

	{case default="yes"}
	{exp:calendar:occurrences event_id="{event_id}" occurrence_id="{segment_4}" occurrences_limit="1"}
	<tr>
		<th>Date</th>
		<td>
			<p>{occurrence_start_date format="%F %j, %Y"} 
			{if occurrence_all_day != 1} &nbsp;&bull;&nbsp; {occurrence_start_date format="%g:%i %a"}{/if}
			{if occurrence_all_day != 1 OR occurrence_multi_day == 1} &ndash; {/if}
			{if occurrence_multi_day}{occurrence_end_date format="%F %j, %Y"} {/if}
			{if occurrence_all_day == 1 AND occurrence_multi_day == 1} &nbsp;&bull;&nbsp; {/if}
			{if occurrence_all_day != 1}{occurrence_end_date format="%g:%i %a"}{/if}
			</p>
		</td>
	</tr>
	{/exp:calendar:occurrences}
	{/case}

{/exp:switchee}


{if events-host OR events-unlisted_host}

<tr>
	<th>Hosted by</th>
	<td>{if events-host}{events-host}<a href={url_title_path=directory/businesses}>{title}</a>{/events-host}{if:else}{events-unlisted_host}{/if}</td>
</tr>

{/if}

{if events-venue OR events-unlisted_venue}

<tr>
	<th>Venue</th>
	<td>{if events-host}{events-host}<a href={url_title_path=directory/businesses}>{title}</a>{/events-host}{if:else}{events-unlisted_host}{/if}</td>
	{if events-room_name_number}<p>{events-room_name_number}</p>{/if}</td>
</tr>

{/if}

{if events-website_url}
<tr>
	<th>Website</th>
	<td><p><a href="{events-website_url}">{exp:eehive_hacksaw chars="60" append="..."}{events-website_url}{/exp:eehive_hacksaw}</a></p></td>
</tr>
{/if}

{if events-facebook_event_url}
<tr>
	<th>Facebook Event</th>
	<td><p><a href="{events-facebook_event_url}">{events-facebook_event_url}</a></p></td>
</tr>
{/if}

{if events-contact_name OR events-contact_email OR events-contact_phone}

<tr>
	<th>Contact</th>
	<td>{if events-contact_name}{events-contact_name}<br>{/if}
	{if events-contact_phone}{events-contact_phone}<br>{/if}
	{if events-contact_email}<a href="mailto:{events-contact_email}">{events-contact_email}</a>{/if}
	</td>
</tr>

{/if}

{if events-ticket_info}

<tr>
	<th>Ticket Info</th>
	<td>{events-ticket_info}</td>
</tr>
	
{/if}

{if events-audience}

<tr>
	<th>Audience</th>
	<td>{events-audience}</td>
</tr>
	
{/if}

{if events-venue}
{events-venue}
{if directory-map_location}
<tr>
	<th>Map</th>
	<td>
		{exp:gmap:init id="map" class="google-map" style="min-width:300px;height:250px"}
		{directory-map_location id="map" zoom="15"}
	</td>
</tr>
{/if}
{/events-venue}
{/if}


{if events-related_events}
<tr>
	<th>Related Events</th>
	<td>
		<ul>{events-related_events}
			<li><a href="{url_title_path=calendar/event}">{title}</li>
		{/events-related_events}</ul>
	</td>
</tr>
{/if}



</table>

<!--
events-address_line_1: {events-address_line_1}
events-address_line_2: {events-address_line_2}
events-city: {events-city}
events-zip: {events-zip}
-->

{if no_results}
{redirect='404'}
{/if}

{/exp:calendar:events} 