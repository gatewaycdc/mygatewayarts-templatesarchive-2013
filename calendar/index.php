{exp:stash:set type="snippet" parse_vars="yes" parse_tags="yes" parse_conditionals="yes"}
	
	{stash:page_color_context}
		{if segment_1 == "play"}orange_page{/if}
		{if segment_1 == "work"}green_page{/if}
		{if segment_1 == "live"}blue_page{/if}
	{/stash:page_color_context}
	
	{stash:main_content}
	<p align=center><img src="http://mygatewayarts.org/media/site_media/blueArrow2.png" border="0"> <font size="1">Find user-submitted events featuring Gateway artists and businesses at local <a href="http://mygatewayarts.org/businesses">galleries, restaurants, theatres, stores, parks, and other venues</a>, both in and outside of the Arts District. <i>Learn more <a href="http://mygatewayarts.org/play/discover-things-to-do/submit-an-event-to-our-calendar" target="_blank">here</a>.</i></font></p><br>

		<div id="calendar"></div>
		
		<script type='text/javascript'>  
		$(document).ready(function() {
			$('#calendar').fullCalendar({
				header: {
					left: 'prev,next today',
					center: 'title',
					right: 'month,agendaWeek,agendaDay'
				},
				editable: false,
				events: [ {}

				{embed="calendar/.all_events_json"}

				]
			});  
		}); 
		</script>
	
	{/stash:main_content}

	{stash:sidebar_content}
	
	{/stash:sidebar_content}

{/exp:stash:set}

{embed="pages/.story_page_scaffold-full_width"}