{exp:stash:set type="snippet" parse_vars="yes" parse_tags="yes"}
	
	{stash:page_color_context}orange_page{/stash:page_color_context}
	
	{stash:page_title}Calendar{/stash:page_title}

	{stash:main_content}
	
	{embed="calendar/.event_detail"}

	{/stash:main_content}
	
	{stash:sidebar_content}
		
		{exp:structure:nav add_unique_ids="yes" add_level_classes="yes" css_class="sidebar_nav" show_depth="2" start_from="play" exclude="24"}

		{exp:structure:nav add_unique_ids="yes" add_level_classes="yes" css_class="sidebar_nav" show_depth="2" start_from="/play" include="24"}
	
	{/stash:sidebar_content}

{/exp:stash:set}

{embed="pages/.story_page_scaffold"}