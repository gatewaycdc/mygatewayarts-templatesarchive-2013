{exp:calendar:events sort="asc" dynamic="off" limit="999"}
	{occurrences}
			,{
				title: "{exp:low_replace find="QUOTE|NEWLINE" replace="\QUOTE|SPACE" multiple="yes"}{event_title}{/exp:low_replace}",
				url: '{url_title_path="calendar/event"}/{occurrence_id}',
				{if event_all_day}
				start: new Date({occurrence_start_date format="%Y,%n-1,%j,%G,%i "}),
				end: new Date({occurrence_end_date format="%Y,%n-1,%j,%G,%i-1 "}),
				allDay: true
				{if:else}
				start: new Date({occurrence_start_date format="%Y,%n-1,%j,%G,%i "}),
				end: new Date({occurrence_end_date format="%Y,%n-1,%j,%G,%i"}),
				allDay: false
				{/if}
			}
	{/occurrences}
{/exp:calendar:events}