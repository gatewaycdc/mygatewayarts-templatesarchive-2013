/*-------------------------------------------------------------------- 
 * equalHeights / equalWidths
 * by Michael Rog
 *
 * based on the JQuery Plugins "EqualHeights" & "EqualWidths"
 * by Scott Jehl, Todd Parker, Maggie Costello Wachs (http://www.filamentgroup.com)
 *--------------------------------------------------------------------*/

$.fn.equalHeights = function(mode,set) {
	var currentTallest = 0;
	(set ? $(set) : $(this)).each(function(i){
		// window.log("equalHeights: comparing: " + $(this).height() + " vs " + currentTallest);
		if ($(this).height() > currentTallest) { currentTallest = $(this).height(); }
	});
	// for ie6, set height since min-height isn't supported
	// window.log("equalHeights: max height for this set is " + currentTallest);
	if ($.browser.msie && $.browser.version == 6.0) { $(this).css({'height': currentTallest}); }
	$(this).css({'min-height': currentTallest}).addClass("equalHeights_applied");
	return this;
};

$.fn.equalWidths = function(mode,set) {
	var currentTallest = 0;
	(set ? $(set) : $(this)).each(function(i){
		// window.log("equalWidths: comparing: " + $(this).width() + " vs " + currentTallest);
		if ($(this).width() > currentTallest) { currentTallest = $(this).width(); }
	});
	// for ie6, set width since min-width isn't supported
	// window.log("equalWidths: max width for this set is " + currentTallest);
	if ($.browser.msie && $.browser.version == 6.0) { $(this).css({'width': currentTallest}); }
	$(this).css({'min-width': currentTallest}).addClass("equalWidths_applied");
	return this;
};