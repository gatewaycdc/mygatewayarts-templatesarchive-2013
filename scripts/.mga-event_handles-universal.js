//<![CDATA[

FunctionHandler.register( '*', function(){
	
	// ================= COLUMN HEIGHT FIXES =================

	try
	{
		$("#main, #sidebar").equalHeights();
		// window.log("equalheights");
	}
	catch(err)
	{
		window.log("ERROR in COLUMN HEIGHT FIXES: " + err.message);
	}
	
	// ==================================

	// ================= ACCORDIONS =================

	try
	{
	}	
	catch(err)
	{
		window.log("ERROR in ACCORDIONS: " + err.message);
	}
	
	// ==================================

	// ================= DIRECTORY LISTING IMAGES LIGHTBOX =================

	try
	{
	
		$(".lightbox").fancybox({
			openEffect	: 'none',
			closeEffect	: 'none'
			});
	
	}	
	catch(err)
	{
		window.log("ERROR in DIRECTORY LISTING IMAGES LIGHTBOX: " + err.message);
	}
	
	// ==================================
	
	// ================= OPEN EXTERNAL LINKS IN NEW TABS =================

	try
	{
	
		$("a").not("[href*='mygatewayarts.org']").not("[href^='#']").not("[href^='/']")
			.addClass("external_link")
			.attr({ target: "_blank" });
		
	}
	catch(err)
	{
		window.log("OPEN EXTERNAL LINKS IN NEW TABS: " + err.message);
	}
	
	// ==================================

	// ================= STUDIO LISTING =================

	try
	{
	
		// $('#studio_listing_table').dataTable();
		
	}
	catch(err)
	{
		window.log("STUDIO LISTING: " + err.message);
	}
	
	// ==================================	
	
	// ================= EVENT EDIT SCREEN =================

	// $("[name*='events-host[s']").last().val()
	// #events-host, 

	try
	{
		

	}
	catch(err)
	{
		window.log("ERROR in EVENT EDIT SCREEN: " + err.message);
	}
	
	
	
});

//]]>