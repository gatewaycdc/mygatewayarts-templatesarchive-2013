/* ============================================================================= */
/* =====------===  library scripts  ===------===== */

; {!-- {embed="scripts/.lib-jquery-171-min"} --}
; {embed="scripts/.lib-bootstrap"}


/* ============================================================================= */
/* =====------===  plugin scripts  ===------===== */

; {embed="scripts/.plugin-html5_boilerplate"}
; {embed="scripts/.plugin-jquery-function_handler"}
; {embed="scripts/.plugin-jquery-equalheights"}
; {embed="scripts/.plugin-jquery-easing-13-min"}
; {embed="scripts/.plugin-jquery-royalslider-81"}
; {embed="scripts/.plugin-jquery-fullcalendar"}
; {embed="scripts/.plugin-jquery-fullcalendar-gcal"}
; {embed="scripts/.plugin-jquery-mousewheel-306-pack"}
; {embed="scripts/.plugin-jquery-fancybox"}
; {embed="scripts/.plugin-jquery-datatables"}


/* ============================================================================= */
/* =====------===  site scripts  ===------===== */

; {embed="scripts/.mga-event_handles-universal"}
;

// ================= GOOGLE ANALYTCS =================

	var _gaq = _gaq || [];
	_gaq.push(['_setAccount', 'UA-39880524-1']);
	_gaq.push(['_trackPageview']);
	_gaq.push(['_setAccount', 'UA-32111180-1']);
	_gaq.push(['_trackPageview']);
	
	(function() {
		var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	})();

// ==================================