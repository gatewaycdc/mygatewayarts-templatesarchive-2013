<h1>Artists Directory</h1>

{exp:switchee parse="inward" variable="{embed:post:medium_categories}"}

{case value=""}

<p class="breadcrumb"><a href="/">Home</a> &rarr; <a href="/directory/artists">Directory</a> &rarr; Artists</p>

{!-- In sn.directory.listing_item, each actual map marker creates an entry in this list. Then we retrieve the list, and only initialize the map if there are markers to be displayed. Crucially, this happens later in the parse order. --}
{exp:stash:get_list name="map_items" process="end" limit="1" parse_conditionals="yes" parse_tags="yes"}
	{if display_map}{exp:gmap:init id="map" class="google-map" style="width:600px;height:300px"}{/if}
{/exp:stash:get_list}

{exp:channel:entries channel="directory" dynamic="off" disable="{sn.disable_most}" category="76" orderby="title" sort="asc|asc" limit="999"}

{sn.directory.listing_item}
{if no_results}Sorry, we can't find any listings that match your search.{/if}

{/exp:channel:entries}

{/case}

{case default="yes"}

<p class="breadcrumb"><a href="/">Home</a> &rarr; <a href="/directory">Directory</a> &rarr; <a href="/directory/artists">Artists</a> &rarr; {exp:channel:categories style="linear" show="{embed:post:medium_categories}"}{category_name}{/exp:channel:categories}</p>

{!-- In sn.directory.listing_item, each actual map marker creates an entry in this list. Then we retrieve the list, and only initialize the map if there are markers to be displayed. Crucially, this happens later in the parse order. --}
{exp:stash:get_list name="map_items" process="end" limit="1" parse_conditionals="yes" parse_tags="yes"}
	{if display_map}{exp:gmap:init id="map" class="google-map" style="width:600px;height:300px"}{/if}
{/exp:stash:get_list}

{exp:channel:entries channel="directory" dynamic="off" disable="{sn.disable_most}" category="76&{embed:post:medium_categories}" orderby="title|directory-trade_name" sort="asc|asc" limit="999"}

{sn.directory.listing_item}
{if no_results}Sorry, we can't find any listings that match your search.{/if}

{/exp:channel:entries}

{/case}

{/exp:switchee}