<h1>Business Directory</h1>

{exp:switchee parse="inward" variable="{segment_3}"}

{case value=""}

<p class="breadcrumb"><a href="/">Home</a> &rarr; <a href="/directory/businesses">Directory</a> &rarr; Businesses</p>

{!-- In sn.directory.listing_item, each actual map marker creates an entry in this list. Then we retrieve the list, and only initialize the map if there are markers to be displayed. Crucially, this happens later in the parse order. --}
{exp:stash:get_list name="map_items" process="end" limit="1" parse_conditionals="yes" parse_tags="yes"}
	{if display_map}{exp:gmap:init id="map" class="google-map" style="width:600px;height:300px"}{/if}
{/exp:stash:get_list}

{exp:channel:entries channel="directory" dynamic="off" disable="{sn.disable_most}" category="75" orderby="directory-trade_name|title" sort="asc|asc" limit="999"}

{if count == 1}{/if}

{sn.directory.listing_item}
{if no_results}Sorry, we can't find any listings that match your search.{/if}

{/exp:channel:entries}

{/case}

{case value="search"}

<p class="breadcrumb"><a href="/">Home</a> &rarr; <a href="/directory">Directory</a> &rarr; <a href="/directory/businesses">Businesses</a> &rarr; Search Results</p>

{!-- In sn.directory.listing_item, each actual map marker creates an entry in this list. Then we retrieve the list, and only initialize the map if there are markers to be displayed. Crucially, this happens later in the parse order. --}
{exp:stash:get_list name="map_items" process="end" limit="1" parse_conditionals="yes" parse_tags="yes"}
	{if display_map}{exp:gmap:init id="map" class="google-map" style="width:600px;height:300px"}{/if}
{/exp:stash:get_list}

{exp:low_search:results query="{segment_4}" disable="{sn.disable_most}" category="75" orderby="directory-trade_name|title" sort="asc|asc" limit="999" collection="directory_search"}

{if count ==  1}{/if}

{sn.directory.listing_item}
{if low_search_no_results}Sorry, we can't find any listings that match your search.{/if}

{/exp:low_search:results}

{/case}

{/exp:switchee}