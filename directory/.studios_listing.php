{exp:switchee parse="inward" variable=""}

{case value=""}

<p class="breadcrumb"><a href="/">Home</a> &rarr; <a href="/directory/studios">Directory</a> &rarr; Studios for Lease</p>
<h1>Studios for Lease</h1>

{!-- {exp:gmap:init id="map" class="google-map" style="width:600px;height:300px"} --}

<table class="table" id="studio_listing_table" style="margin-bottom:24px">

<thead>
<tr>
<th>Title</th>
<th>Sq.&nbsp;Ft.</th>
<th>Location</th>
<th></th>
</tr>
</thead>

<tbody>
{exp:channel:entries channel="studio_listings" dynamic="off" disable="{sn.disable_most}"}
<tr>

<td>{title}</td>
<td>{studio_listings-sqft}</td>
<td>
	{if studio_listings-business_listing}
		{studio_listings-business_listing}
			{if directory-trade_name}{directory-trade_name}
			{if:else}{title}
			{/if}
		{/studio_listings-business_listing}
	{if:else}
		{studio_listings-alternate_addres}
	{/if}
</td>
<td>
	<a href="{url_title_path=directory/studios}" class="btn">See&nbsp;more&nbsp;details</a>
</td>

</tr>
{/exp:channel:entries}
</tbody>


{!-- }

<thead>
<tr>
<th>Title</th>
<th>Sq. Ft.</th>
<th>Industrial Electrical</th>
<th>Ventilation</th>
<th>Haz. mat.</th>
<th>Handicap</th>
<th>Restroom</th>
<th>Sink</th>
<th>24-hr access</th>
<th>24-hr heating</th>
<th>24-hr cooling</th>
<th>Telephone service</th>
<th>Internet service</th>
</tr>
</thead>

<tbody>
{exp:channel:entries channel="studio_listings" dynamic="off" disable="{sn.disable_most}"}
<tr>

<td>{title}</td>
<td>{studio_listings-sqft}</td>
<td class="center">{if studio_listings-industrial_elect == 'y'}<i class="icon-ok"></i>{/if}</td>
<td class="center">{if studio_listings-ventilation == 'y'}<i class="icon-ok"></i>{/if}</td>
<td class="center">{if studio_listings-hazardous_materi == 'y'}<i class="icon-ok"></i>{/if}</td>
<td class="center">{if studio_listings-handicap_accessi == 'y'}<i class="icon-ok"></i>{/if}</td>
<td class="center">{if studio_listings-restroom == 'y'}<i class="icon-ok"></i>{/if}</td>
<td class="center">{if studio_listings-sink == 'y'}<i class="icon-ok"></i>{/if}</td>
<td class="center">{if studio_listings-24_hour_access == 'y'}<i class="icon-ok"></i>{/if}</td>
<td class="center">{if studio_listings-24_hour_heating == 'y'}<i class="icon-ok"></i>{/if}</td>
<td class="center">{if studio_listings-24_hour_cooling == 'y'}<i class="icon-ok"></i>{/if}</td>
<td class="center">{if studio_listings-telephone_servic == 'provided'}<i class="icon-ok"></i>{/if}</td></td>
<td class="center">{if studio_listings-internet_service == 'provided'}<i class="icon-ok"></i>{/if}</td>

</tr>
{/exp:channel:entries}
</tbody>

--}

</table>

{/case}

{case default="yes"}
{/case}

{/exp:switchee}