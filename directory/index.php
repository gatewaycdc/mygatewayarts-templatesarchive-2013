{exp:stash:set type="snippet" parse_vars="yes" parse_tags="yes"}
	
	{stash:page_color_context}
		{if segment_1 == "play"}orange_page{/if}
		{if segment_1 == "work"}green_page{/if}
		{if segment_1 == "live"}blue_page{/if}
	{/stash:page_color_context}

	{stash:main_content}

		{if 1}
			<p class="breadcrumb">{exp:structure:breadcrumb separator="&rarr;" here_as_title="yes" wrap_separator='span class="dark_grey"'}</p>
		{/if}
		
		{exp:channel:entries require_entry="yes" disable="{sn.disable_most}"}
		
		{stash:page_title}{title}{/stash:page_title}
		<h1>{title}</h1>
		
		<div>
		{if content_pages-content}
		{content_pages-content}
		{if:else}
		<p class="alert alert-error">This page doesn't have any content yet.</p>
		{/if}
		</div>
		
		{embed="dev-content/.sign_off"}
		
		{/exp:channel:entries}

	{/stash:main_content}
	
	{stash:sidebar_content}
	
		{sn.sidebar}
	
	{/stash:sidebar_content}

{/exp:stash:set}

{embed="pages/.story_page_scaffold"}