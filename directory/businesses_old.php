{exp:stash:set type="snippet" parse_vars="yes" parse_tags="yes"}
	
	{stash:page_color_context}orange_page{/stash:page_color_context}
	
	{stash:page_title}Businesses Directory{/stash:page_title}

	{stash:main_content}

		{if segment_3}{embed="directory/.business_detail"}{/if}
		{if segment_3 == ""}{embed="directory/.business_listing"}{/if}

	{/stash:main_content}
	
	{stash:sidebar_content}
		
		{sn.directory.business_controls}
		
		{sn.directory.sidebar}
	
	{/stash:sidebar_content}

{/exp:stash:set}

{embed="pages/.story_page_scaffold"}