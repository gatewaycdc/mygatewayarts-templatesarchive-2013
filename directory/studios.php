{exp:stash:set type="snippet" parse_vars="yes" parse_tags="yes"}
	
	{stash:page_color_context}green_page{/stash:page_color_context}
	
	{stash:page_title}Studios Directory{/stash:page_title}

	{stash:main_content}

		{if segment_3 == "" OR segment_3 =="search"}{embed="directory/.studios_listing"}
		{if:else}{embed="directory/.studios_detail"}{/if}
		
	{/stash:main_content}
	
	{stash:sidebar_content}
		
		{sn.directory.studio_controls}
		
		{sn.directory.sidebar}
	
	{/stash:sidebar_content}

{/exp:stash:set}

{embed="pages/.story_page_scaffold-full_width"}