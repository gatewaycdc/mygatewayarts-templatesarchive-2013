{exp:channel:entries channel="studio_listings" require_entry="yes" disable="{sn.disable_most}"}

<p class="breadcrumb"><a href="/">Home</a> &rarr; <a href="/directory">Directory</a> &rarr; <a href="/directory/studios">Studios</a> &rarr; {title}</p>

<div style="float:right;margin-top:24px;">{sn.edit_this}</div>
<h1>{title}</h1>

<div class="g_c">

<div class="g_8 g_a">

{if studio_listings-description}
<h4>Description</h4>
<div style="margin-bottom:24px;">{studio_listings-description}</div>
{/if}

<table class="table">

<tr>
	<th>Location</th>
	<td>
		{if studio_listings-business_listing}
			{studio_listings-business_listing}
				{if directory-trade_name}{directory-trade_name}
				{if:else}{title}
				{/if}
			{/studio_listings-business_listing}
		{if:else}
			{studio_listings-alternate_addres}
		{/if}
	</td>
</tr>

{if studio_listings-sqft}
	<tr>
	<th>Sq. Ft.</th>
	<td>
	{studio_listings-sqft}
	</td>	
	</tr>
{/if}

{if studio_listings-contact_name OR studio_listings-contact_phone OR studio_listings-contact_email}
	<tr>
	<th>Contact</th>
	<td>
	{if studio_listings-contact_name}{studio_listings-contact_name}<br>{/if}
	{if studio_listings-contact_phone}{studio_listings-contact_phone}<br>{/if}
	{if studio_listings-contact_email}<a href="mailto:{studio_listings-contact_email}">{studio_listings-contact_email}</a>{/if}
	</td>	
	</tr>
{/if}

</table>

</div><!-- / .g_8 .g_a -->

<div class="g_7 g_o">

<h4>Amenities</h4>

<table class="table">

	<tr>
		<th>Industrial Electrical
</th>
		
		<td>
		{if studio_listings-industrial_elect == "y"}
		<i class="icon-ok"> </i>
		{/if}
		</td>
	</tr>

	<tr>
		<th>Industrial Ventilation</th>
		<td>
		{if studio_listings-ventilation == "y"}
		<i class="icon-ok"> </i> 
		{/if}
		</td>
	</tr>

	<tr>
		<th>Hazardous materials allowed</th>
		<td>
		{if studio_listings-hazardous_materi == "y"}
		<i class="icon-ok"> </i> 
		{/if}
		</td>
	</tr>
	
	<tr>
		<th>Handicap Accessible</th>
		<td>
		{if studio_listings-handicap_accessi == "y"}
		<i class="icon-ok"> </i> 
		{/if}
		</td>
	</tr>
	
	<tr>
		<th>Restroom</th>
		<td>
		{if studio_listings-restroom == "y"}
		<i class="icon-ok"> </i> 
		{/if}
		</td>
	</tr>
	
	<tr>
		<th>Sink</th>
		<td>
		{if studio_listings-sink == "y"}
		<i class="icon-ok"> </i> 
		{/if}
		</td>
	</tr>

	<tr>
		<th>24-hour Access</th>
		<td>
		{if studio_listings-24_hour_access == "y"}
		<i class="icon-ok"> </i> 
		{/if}
		</td>
	</tr>
	<tr>
		<th>24-hour Heating</th>
		<td>
		{if studio_listings-24_hour_heating == "y"}
		<i class="icon-ok"> </i> 
		{/if}
		</td>
	</tr>

	<tr>
		<th>24-hour Cooling</th>
		<td>
		{if studio_listings-24_hour_cooling == "y"}
		<i class="icon-ok"> </i> 
		{/if}
		</td>
	</tr>
	
	<tr>
		<th>Parking</th>
		<td>
		{if studio_listings-parking}
		<i class="icon-ok"> </i> 
			{if studio_listings-parking == "street"}&nbsp; Street parking
			{if:elseif studio_listings-parking == "private"}&nbsp; Private parking
			{if:else}&nbsp; Street &amp; private parking
			{/if}
		{/if}
		</td>
	</tr>
	
	<tr>
		<th>Telephone service</th>
		<td>
		{if studio_listings-telephone_servic != "" AND studio_listings-telephone_servic != "none"}
		<i class="icon-ok"> </i> 
			{if studio_listings-telephone_servic == "available"}&nbsp; Available{/if}
			{if studio_listings-telephone_servic == "provided"}&nbsp; Provided{/if}
		{/if}
		</td>
	</tr>
	
	<tr>
		<th>Internt service</th>
		<td>
		{if studio_listings-internet_service != "" AND studio_listings-internet_service != "none"}
		<i class="icon-ok"> </i> 
			{if studio_listings-internet_service == "available"}&nbsp; Available{/if}
			{if studio_listings-internet_service == "provided"}&nbsp; Provided{/if}
		{/if}
		</td>
	</tr>
	
</table>

</div><!-- / .g_7 g_o -->

</div><!-- / .g_c -->

<div style="float:right;margin-top:24px;">{sn.edit_this}</div>

{!--

{studio_listings-business_listing}
{studio_listings-alternate_addres}


{studio_listings-description}
{studio_listings-contact_name}
{studio_listings-contact_phone}
{studio_listings-contact_email}
{studio_listings-sqft}

{studio_listings-industrial_elect}
{studio_listings-ventilation}
{studio_listings-hazardous_materi}
{studio_listings-handicap_accessi}
{studio_listings-restroom}
{studio_listings-sink}
{studio_listings-24_hour_heating}
{studio_listings-24_hour_cooling}
{studio_listings-telephone_servic}
{studio_listings-internet_service}

--}

{if no_results}<p class="alert alert-error">Sorry, we couldn't find a Studio Listing here. You can view the full directory of Studio Listings <a href="/directory/studios">here</a>.</p>{/if}

{/exp:channel:entries}