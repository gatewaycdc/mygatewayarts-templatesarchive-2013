<h1>Business Directory</h1>

{exp:switchee parse="inward" variable="{embed:post:business_categories}"}

{case value=""}

<p class="breadcrumb"><a href="/">Home</a> &rarr; <a href="/directory/businesses">Directory</a> &rarr; Businesses</p>

{exp:gmap:init id="map" class="google-map" style="width:600px;height:300px"}

{exp:channel:entries channel="directory" dynamic="off" disable="{sn.disable_most}" category="75" orderby="directory-trade_name|title" sort="asc|asc" limit="999"}

{sn.directory.listing_item}
{if no_results}Sorry, we can't find any listings that match your search.{/if}

{/exp:channel:entries}

{/case}

{case default="yes"}

<p class="breadcrumb"><a href="/">Home</a> &rarr; <a href="/directory">Directory</a> &rarr; <a href="/directory/businesses">Businesses</a> &rarr; {exp:channel:categories style="linear" show="{embed:post:business_categories}"}{category_name}{/exp:channel:categories}</p>

{exp:gmap:init id="map" class="google-map" style="width:600px;height:300px"}

{exp:channel:entries channel="directory" dynamic="off" disable="{sn.disable_most}" category="75&{embed:post:business_categories}" orderby="directory-trade_name|title" sort="asc|asc" limit="999"}

{sn.directory.listing_item}
{if no_results}Sorry, we can't find any listings that match your search.{/if}

{/exp:channel:entries}

{/case}

{/exp:switchee}