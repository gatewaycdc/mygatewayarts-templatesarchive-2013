{exp:stash:set type="snippet" parse_vars="yes" parse_tags="yes"}
	
	{stash:page_color_context}green_page{/stash:page_color_context}
	
	{stash:page_title}Artists Directory{/stash:page_title}

	{stash:main_content}
	
	{if segment_3}{embed="directory/.artists_detail"}{/if}
	{if segment_3 == ""}{embed="directory/.artists_listing"}{/if}

	{/stash:main_content}
	
	{stash:sidebar_content}
		
		{sn.directory.artist_controls}
		
		{sn.directory.sidebar}
	
	{/stash:sidebar_content}

{/exp:stash:set}

{embed="pages/.story_page_scaffold"}