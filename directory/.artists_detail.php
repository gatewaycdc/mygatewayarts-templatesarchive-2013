{exp:channel:entries channel="directory" require_entry="yes" disable="{sn.disable_most}" category="76"}

<p class="breadcrumb"><a href="/">Home</a> &rarr; <a href="/directory/artists">Directory</a> &rarr; <a href="/directory/artists">Artists</a> &rarr; {title}</p>

<div style="float:right;margin-top:24px;">{sn.edit_this}</div>
<h1>{title}</h1>

{if directory-about}
<h4>About</h4>
<div style="margin-bottom:24px;">{directory-about}</div>
{/if}

{if directory-artist_statement}
<h4>Artist's Statement/Focus</h4>
<div style="margin-bottom:24px;">{directory-artist_statement}</div>
{/if}

{if directory-images}
	
	<ul class="thumbnails">
	{exp:channel_images:images entry_id="{entry_id}" parse="inward"}

		<li class="">
		<a href="{exp:ce_img:pair src="{image:url}" width="500"}{made}{/exp:ce_img:pair}" rel="listing_{entry_id}_photos" class="thumbnail lightbox" title="{image:title}">
		{exp:ce_img:single src="{image:url}" width="90"}
		</a>
		</li>

	{/exp:channel_images:images}
	</ul>

{/if}

{if directory-files}
	
	<ul>
	{exp:channel_files:files entry_id="{entry_id}" parse="inward"}

		<li class="">
		<a href="{file:url}" title="{file:title}">
			{file:title}
		</a>
		</li>

	{/exp:channel_files:files}
	</ul>

{/if}

<table class="table">

{if directory-email}
	<tr>
	<th>E-mail</th>
	<td>{directory-email}</td>
	</tr>
{/if}

{if directory-phone}
	<tr>
	<th>Phone</th>
	<td>{directory-phone}</td>
	</tr>
{/if}

{if directory-website_url OR directory-facebook_url OR directory-twitter_handle OR directory-linkedin_url}
	<tr>
	<th>Web Links</th>
	<td>{if directory-website_url}<a href="{directory-website_url}">{directory-website_url}</a><br>{/if}
	{if directory-facebook_url}<a href="{directory-facebook_url}">{directory-facebook_url}</a><br>{/if}
	{if directory-twitter_handle}@<a href="http://twitter.com/{directory-twitter_handle}">{directory-twitter_handle}</a>{/if}<br>
	{if directory-linkedin_url}<a href="{directory-linkedin_url}">{directory-linkedin_url}</a>{/if}
	</td>
	</tr>
{/if}

{if directory-street_address OR directory-address_description OR directory-city OR directory-zip}
	<tr>
	<th>Location</th>
	<td>
	{if directory-street_address}{directory-street_address}<br>{/if}
	{if directory-address_description}{directory-address_description}<br>{/if}
	{if directory-city}{directory-city}<br>{/if}
	{if directory-zip}{directory-zip}{/if}
	</td>
	</tr>
{/if}

</table>

<div style="float:right;margin-top:24px;">{sn.edit_this}</div>

{!--

<!-- 
{directory-images}
{directory-map_location}
-->

<!--
directory-trade_name: {directory-trade_name}
directory-email: {directory-email}
directory-phone: {directory-phone}
directory-website_url: {directory-website_url}
directory-facebook_url: {directory-facebook_url}
directory-twitter_handle: {directory-twitter_handle}
directory-linkedin_url: {directory-linkedin_url}
directory-reviews_url: {directory-reviews_url}
directory-business_handicap_acce: {directory-business_handicap_acce}
directory-business_has_regular_h: {directory-business_has_regular_h}
directory-business_holds_regular: {directory-business_holds_regular}
directory-business_hours: {directory-business_hours}
directory-business_taxpayer_id_e: {directory-business_taxpayer_id_e}
directory-business_501c3: {directory-business_501c3}
directory-about: {directory-about}
directory-artist_statement: {directory-artist_statement}
directory-images: {directory-images}
directory-street_address: {directory-street_address}
directory-address_description: {directory-address_description}
directory-city: {directory-city}
directory-zip: {directory-zip}
directory-map_location: {directory-map_location}
directory-user_notes: {directory-user_notes}
directory-internal_notes: {directory-internal_notes}
directory-import_tags: {directory-import_tags}
directory-import_row_number: {directory-import_row_number}
directory-import_first_name: {directory-import_first_name}
directory-import_last_name: {directory-import_last_name}
directory-import_phone: {directory-import_phone}
directory-import_phone_2: {directory-import_phone_2}
directory-import_alt_zip: {directory-import_alt_zip}
directory-import_email: {directory-import_email}
directory-import_sun_open: {directory-import_sun_open}
directory-import_sun_close: {directory-import_sun_close}
directory-import_mon_open: {directory-import_mon_open}
directory-import_mon_close: {directory-import_mon_close}
directory-import_tue_open: {directory-import_tue_open}
directory-import_tue_close: {directory-import_tue_close}
directory-import_wed_open: {directory-import_wed_open}
directory-import_wed_close: {directory-import_wed_close}
directory-import_thu_open: {directory-import_thu_open}
directory-import_thu_close: {directory-import_thu_close}
directory-import_fri_open: {directory-import_fri_open}
directory-import_fri_close: {directory-import_fri_close}
directory-import_sat_open: {directory-import_sat_open}
directory-import_sat_close: {directory-import_sat_close}
directory-import_business_type: {directory-import_business_type}
directory-import_medium: {directory-import_medium}
-->

--}

{if no_results}<p class="alert alert-error">Sorry, we couldn't find an Artist here. You can view the full directory of Artists <a href="/directory/artists">here</a>.</p>{/if}

{/exp:channel:entries}