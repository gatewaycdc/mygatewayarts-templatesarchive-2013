<!DOCTYPE html>  

<html lang="en">  

	<head>

		<meta charset="utf-8">
		<title>Page Not Found | MyGatewayArts</title>  
		
		<style>

		* {
			margin: 0;
			padding: 0;
			border: 0;
		}

		body {
			font: sans-serif;
			background-color: #fdf1e4;
			padding: 10%;
			text-align: center;
		}
		
		a {
			color: black;
			outline: none;
			text-decoration: underline;
		}
		a:hover {
			text-decoration: none;
		}
		
		</style>

	</head>

	<body>
	
		<a href="http://mygatewayarts.org/live/news-about-the-arts-district/site-map"><img src="/media/design_assets/404.gif" border="0"></a>
<br><br><font face="Arial">Sorry! The page you're looking for doesn't exist. Click <a href="http://mygatewayarts.org/live/news-about-the-arts-district/site-map">here</a> to visit our site map.</font>
	</body>

</html>