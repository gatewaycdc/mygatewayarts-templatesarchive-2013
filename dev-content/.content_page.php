{sn.page_open}

<div class="g_c content_wrapper">
<div class="g_12">

{exp:channel:entries require_entry="yes" disable="{sn.disable_most}"}

<h1>{title}</h1>

<div>
{if content_pages-content}
{content_pages-content}
{if:else}
<p class="alert alert-error">This page doesn't have any content yet.</p>
{/if}
</div>

{embed="dev-content/.sign_off"}

{/exp:channel:entries}

</div><!-- / .g_12 -->
</div><!-- / .g_c -->

{sn.page_close}