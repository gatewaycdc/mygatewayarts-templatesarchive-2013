{if member_group == "1"}

{exp:channel:entries require_entry="yes" disable="{sn.disable_most}"}

<div class="well vertical_margin">

<p>{exp:structure:breadcrumb here_as_title="yes"}</p>

{sn.edit_this}

{if content_pages-is_signed_off == "y"}

<p class="alert alert-success">This page is marked as SIGNED OFF!</p>

{exp:safecracker channel="content_pages" entry_id="{entry_id}" return="{structure:page:uri}"}
<input name="content_pages-is_signed_off" type="hidden" value="in-progress">
<input type="submit" class="btn" value="Mark as in-progress">
{/exp:safecracker}

{if:else}

<p class="alert">This page is still in-progress.</p>

{exp:safecracker channel="content_pages" entry_id="{entry_id}" return="{structure:page:uri}"}
<input name="content_pages-is_signed_off" type="hidden" value="y">
<input type="submit" class="btn" value="Mark as signed off">
{/exp:safecracker}

{/if}

</div><!-- / .well -->

{/exp:channel:entries}

{/if}