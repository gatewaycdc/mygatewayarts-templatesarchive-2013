{sn.page_open}

{sn.mga.masthead}
{sn.paint}

<div class="content_wrapper clearfix">

<div id="main">
<div class="main_content">
	
	{main_content}

</div><!-- / .main_content -->
</div><!-- / #main -->

<div id="sidebar">
<div class="sidebar_content">

	{sidebar_content}

</div><!-- / .sidebar_content -->
</div><!-- / #sidebar -->

</div><!-- / .content_wrapper -->

{sn.mga.footer}

{sn.page_close}

<!-- template debug note - end template in pages/.story_page_scaffold - {current_time format="%Y-%m-%d %H:%i:%s"} - member {member_id} -->