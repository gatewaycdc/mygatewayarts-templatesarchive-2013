{sn.page_open}

{sn.mga.masthead}

<div class="homepage_paint">

<div class="g_c">

<div class="g_4">

	<h1 class="white">Welcome to the Gateway Arts District!</h1>

	<div class="homepage_flag">
		<a href="/work/find-artists-in-the-district" class="arrow">Find an Artist</a>
	</div>

	<div class="homepage_flag">
		<a href="/play/explore-places-to-go" class="arrow">Find a Business</a>
	</div>

	<div class="homepage_flag">
		<a href="/work/find-a-work-space" class="arrow">Find a Studio</a>
	</div>

</div><!-- / .g_4 -->

<div class="g_8">

	<div class="homepage_nav">
	
		<div class="homepage_nav_block first">
		
			<h2 class="orange"><a class="orange" href="/play">Play</a></h2>
			
			{exp:structure:nav add_unique_ids="yes" add_level_classes="yes" css_class="homepage_nav_list block_list" max_depth="1" show_depth="1" start_from="/play" exclude="461"}
			
			<div class="homepage_nav_photo">
			
			{exp:low_variables:pair var="lv_homepage_play_image" limit="1" sort="random"}
				{if mga_page}<a href="{mga_page}">
				{if:elseif external_url}<a href="{external_url}">
				{/if}
					{exp:ce_img:single src="{asset}" width="184"}
				{if mga_page OR external_url}</a>{/if}
			{/exp:low_variables:pair}
			
			</div><!-- / .homepage_nav_photo -->
			
		</div>

		<div class="homepage_nav_block">
		
			<h2 class="green"><a class="green" href="/work">Work</a></h2>
			
			{exp:structure:nav add_unique_ids="yes" add_level_classes="yes" css_class="homepage_nav_list block_list" max_depth="1" show_depth="1" start_from="/work"}
			
			<div class="homepage_nav_photo">
			
			{exp:low_variables:pair var="lv_homepage_work_image" limit="1" sort="random"}
				{if mga_page}<a href="{mga_page}">
				{if:elseif external_url}<a href="{external_url}">
				{/if}
					{exp:ce_img:single src="{asset}" width="184"}
				{if mga_page OR external_url}</a>{/if}
			{/exp:low_variables:pair}
			
			</div><!-- / .homepage_nav_photo -->
			
		</div>
		
		<div class="homepage_nav_block">
		
			<h2 class="blue"><a class="blue" href="/live">Live</a></h2>
			
			{exp:structure:nav add_unique_ids="yes" add_level_classes="yes" css_class="homepage_nav_list block_list" max_depth="1" show_depth="1" start_from="/live"}
			
			<div class="homepage_nav_photo">
			
			{exp:low_variables:pair var="lv_homepage_live_image" limit="1" sort="random"}
				{if mga_page}<a href="{mga_page}">
				{if:elseif external_url}<a href="{external_url}">
				{/if}
					{exp:ce_img:single src="{asset}" width="184"}
				{if mga_page OR external_url}</a>{/if}
			{/exp:low_variables:pair}
			
			</div><!-- / .homepage_nav_photo -->
			
		</div>
		
	</div><!-- / .homepage_nav -->

</div><!-- / .g_8 -->

</div><!-- / .g_c -->


</div><!-- / .homepage_paint -->

<div class="content_wrapper clearfix">

<div id="homepage_main">
<div class="homepage_main_content">
	
	{main_content}

	<div class="sponsor_logos">
	
		{exp:low_variables:pair var="lv_sponsor_logos"}
			<div class="sponsor_logo">
			{if url}<a href="{url}">{/if}
			{if asset}{exp:ce_img:single src="{asset}" width="200"}{if:else}{url}{/if}
			{if url}</a>{/if}
			</div><!-- / .sponsor_logo -->
		{/exp:low_variables:pair}
	
	</div><!-- / .sponsor_logos -->

</div><!-- / .homepage_main_content -->
</div><!-- / #homepage_main -->

</div><!-- / .content_wrapper -->

{sn.mga.footer}

{sn.page_close}