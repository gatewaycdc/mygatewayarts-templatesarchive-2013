{exp:stash:set type="snippet" parse_vars="yes" parse_tags="yes" refresh="0"}
	
	{stash:page_color_context}
		{if segment_1 == "play"}orange_page{/if}
		{if segment_1 == "work"}green_page{/if}
		{if segment_1 == "live"}blue_page{/if}
	{/stash:page_color_context}

	{stash:main_content}

		<div class="g_c">
		
			<div class="g_5 prefix_1">
				
				<h2 class="splatter_1">News</h2>
				
				{exp:low_variables:pair var="lv_temp_homepage_news"}
				
				{!--
				<ul class="block_list">
				
					<li class="bottom_margin"><p>June 4, 2012<br><strong><a href="http://us4.campaign-archive2.com/?u=0bd66eb46ebf512e4f6d63bcc&id=139ad60cd9">June 2012 Newsletter</a></strong></p></li>
					<li class="bottom_margin"><p>May 12, 2012<br><strong><a href="http://eepurl.com/lJXE1">May 2012 Newsletter</a></strong></p></li>
					<li class="bottom_margin"><p>April 9, 2012<br><strong><a href="http://mygatewayarts.org/news.cfm#008">Under Construction/Revision</a></strong></p></li>
					
				</ul>
				
				<p><a href="/live/news-about-the-arts-district/reading-room-for-press-coverage/reading-room-for-press-coverage/" class="btn arrow">see all news</a></p>
				--}
				{!-- TODO: {structure:page_url_for:472} --}
				
			</div><!-- / .g_6 -->

			<div class="g_5">
			
				<h2 class="splatter_2">Upcoming Events</h2>
				
				<ul class="block_list">
				
				{exp:calendar:cal category="not 47" date_range_start="today" date_range_end="2 weeks" event_limit="4" pad_short_weeks="n"}
					<li class="bottom_margin"><p>{event_start_date format="%l, %F %j"}<br><strong><a href="{url_title_path=calendar/event}">{event_title}</a></strong></p></li>
				{/exp:calendar:cal}
				
				</ul>
				
				<p><a href="/calendar" class="btn arrow">see all events</a> <a href="/play/discover-things-to-do/submit-an-event-to-our-calendar" class="btn arrow">submit an event</a></p>
				<p><br>
	<a href="http://mygatewayarts.org/calendar" target="_blank"><img alt="" src="http://mygatewayarts.org/media/site_media/events-calendar-small-banner.gif" style="width: 300px; height: 60px;" /></a></p>

			</div><!-- / .g_6 -->
		
		</div><!-- / .g_c -->
												
	{/stash:main_content}
	
	{stash:sidebar_content}
	
	{/stash:sidebar_content}

{/exp:stash:set}

{embed="pages/.homepage_scaffold"}