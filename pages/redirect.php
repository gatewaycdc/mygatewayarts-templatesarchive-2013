{exp:channel:entries require_entry="yes" disable="{sn.disable_most}" parse="inward"}

{exp:switchee parse="inward" variable="{channel_short_name}"}

	{case value="redirects"}

		{switchee parse="inward" variable="{redirects-mga_page}"}
					
			{case value=""}
				{!-- If redirects-mga_page is empty, skip this switch and check the external link. --}
			{/case}
			
			{case default="yes"}
				{exp:hs_redirect location="{redirects-mga_page}"}
			{/case}

		{/switchee}
		
		{switchee parse="inward" variable="{redirects-external_url}"}
					
			{case value=""}
				{!-- If redirects-external_url is empty, we have nowhere else to go, so throw the 404. --}
				{redirect=404}
			{/case}
			
			{case default="yes"}
				{exp:hs_redirect location="{redirects-external_url}"}
			{/case}

		{/switchee}

	{/case}
	
	{case default="yes"}
		{!-- This entry isn't in the Redirects channel, so it shouldn't be using this template. --}
		{redirect=404}
	{/case}

{/exp:switchee}

{/exp:channel:entries}