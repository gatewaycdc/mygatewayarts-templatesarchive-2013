{sn.page_open}

{sn.mga.masthead}
{sn.paint}

<div class="content_wrapper clearfix">

<div id="fullwidth_main">
<div class="fullwidth_main_content">
	
	{main_content}

</div><!-- / .fullwidth_main_content -->
</div><!-- / #fullwidth_main -->

</div><!-- / .content_wrapper -->

{sn.mga.footer}

{sn.page_close}