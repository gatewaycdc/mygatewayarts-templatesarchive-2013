{exp:stash:set type="snippet" parse_vars="yes" parse_tags="yes"}
	
	{stash:page_color_context}
		{if segment_1 == "play"}orange_page{/if}
		{if segment_1 == "work"}green_page{/if}
		{if segment_1 == "live"}blue_page{/if}
	{/stash:page_color_context}

	{stash:main_content}

		{if 1}
			<p class="breadcrumb"><a href="{site_url}">Home</a> <span class="dark_grey">&rarr;</span> Search Results</p>
		{/if}
		
		{!--
		
		<h3>Site Search</h3>
		
		{exp:low_search:form collection="{sn.search.site_search_collections}" search_mode="all" result_page="search"}
		<fieldset>
			<p>
			<input type="search" name="keywords" id="keywords" />
			</p>
			<p>
			<input type="submit" class="btn">
			</p>
		</fieldset>
		{/exp:low_search:form}
		
		<hr>
		--}
		
		<h1>Search Results</h1>
		
		<p>Showing results for:&nbsp; <strong><a href="{exp:low_search:url query="{segment_2}"}">{exp:low_search:keywords query="{segment_2}"}</a></strong></p>
		
		{exp:low_search:results query="{segment_2}" limit="10"}
			{if count ==  1}<ol>{/if}
				<li style="margin-bottom: 18px;">
				
				{exp:switchee variable="{channel_short_name}" parse="inward"}
					{case default="yes"}
						<h5><a href="{page_url}">{title}</a></h5>
						<div style="padding-left:12px;">
							<div style="color:#aaa;">{low_search_excerpt}</div>
							<p><strong><a href="{page_url}" class="btn btn-mini">Read more &rarr;</a></strong></p>
						</div>
					{/case}
					{case value="calendar_events"}
						<h5><a href="{url_title_path=calendar/event}">{title}</a></h5>
						<div style="padding-left:12px;">
							<div style="color:#aaa;">{low_search_excerpt}</div>
							<p><strong><a href="{url_title_path=calendar/event}" class="btn btn-mini">Read more &rarr;</a></strong></p>
						</div>
					{/case}
					{case value="directory"}
						<h5><a href="{page_url}">{title}</a></h5>
						<div style="padding-left:12px;">
						<div class="well">
							<p>
								{if directory-trade_name}<strong>{directory-trade_name}</strong><br>{/if}
								{if directory-phone}{directory-phone}<br>{/if}
								{if directory-website_url}<a href="{directory-website_url}">{directory-website_url}</a><br>{/if}
							</p>
							{if directory-about}<p><em>{exp:eehive_hacksaw words="20" append="..."}{directory-about}{/exp:eehive_hacksaw}</em></p>{/if}
							<p><strong><a href="{url_title_path=directory/businesses}" class="btn btn-mini">View Business Listing &rarr;</a></strong></p>
						</div>
						</div>
					{/case}
				{/exp:switchee}
				
				</li>
				<!-- {channel_short_name} -->
			{if count == total_results}</ol>{/if}
			{if low_search_no_results}<p><em>Sorry, no pages match your search.</em></p>{/if}
		{/exp:low_search:results}
		
	{/stash:main_content}
	
	{stash:sidebar_content}

		{sn.sidebar}
			
	{/stash:sidebar_content}

{/exp:stash:set}

{embed="pages/.story_page_scaffold"}