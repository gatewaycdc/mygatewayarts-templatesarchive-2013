<div class="directory_listing_item top_rule">

{if segment_2 == 'businesses' && directory-trade_name != ''}
			<h3><a href="{url_title_path=directory/{segment_2}}">{directory-trade_name}</a></h3>
		{if:else}
			<h3><a href="{url_title_path=directory/{segment_2}}">{title}</a></h3>
		{/if}
{if directory-map_location}
	{directory-map_location id="map" show_one_window="true"}
		{exp:stash:append_list name="map_items"}{stash:display_map}y{/stash:display_map}{/exp:stash:append_list}
		
		{if segment_2 == 'businesses' && directory-trade_name != ''}
			<h3>{directory-trade_name}</h3>
		{if:else}
			<h3>{title}</h3>
		{/if}
	{/directory-map_location}
{/if}

<div class="mb">
<div class="mb_m">
	{exp:channel_images:images entry_id="{entry_id}" cover_only="yes" limit="1"}
	{exp:ce_img:single src="{image:url}" width="100" class="img-frame"}
	{/exp:channel_images:images}
</div>
<div class="mb_b">
	{exp:eehive_hacksaw words="40" append="..."}
	{directory-about}
	{/exp:eehive_hacksaw}
</div>
</div>

<a class="btn" href="{url_title_path=directory/{segment_2}}">Read more</a>

</div><!-- / .directory_listing_item -->