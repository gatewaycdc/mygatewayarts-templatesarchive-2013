{exp:calendar:datepicker_js}
{exp:calendar:datepicker_css}

<div>
	{exp:calendar:date_widget event_id="{segment_3}"}
</div>

<h3>Event details</h3>

<div class="control-group">
	<label class="control-label" for="events-summary">Event Title</label>
	<div class="controls">
		<input type="text" name="title" value="{title}">
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="events-summary">Summary</label>
	<div class="controls">
		{field:events-summary}
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="events-related_events">Related Events</label>
	<div class="controls">
		{field:events-related_events}
	</div>
</div>

<h3>Categories</h3>

<div class="control-group">
	<label class="control-label" for="categories">Event Categories</label>
	<div class="controls">
        <div style="columns: 2; -webkit-columns: 2; -moz-columns: 2;">
        {categories}
	        <label class="checkbox">
	        <input name="category[]" id="categories_{category_id}" type="checkbox" value="{category_id}" {checked}>
	         {category_name}</label>
        {/categories}
        </div>
	</div>
</div>


<h3>Event host</h3>

<div class="control-group">
	<label class="control-label" for="events-summary">Hosted by</label>
	<div class="controls">
		{field:events-host}
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="events-summary">Hosted by (if not listed in the Directory)</label>
	<div class="controls">
		{field:events-unlisted_host}
	</div>
</div>

<h3>Venue info</h3>

<div class="control-group">
	<label class="control-label" for="events-summary">Venue</label>
	<div class="controls">
		{field:events-venue}
	</div>
</div>

<div class="control-group events-host-details">
	<label class="control-label" for="events-summary">Venue (if not listed in the Directory)</label>
	<div class="controls">
		{field:events-unlisted_venue}
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="events-summary">Room name/number</label>
	<div class="controls">
		{field:events-room_name_number}
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="events-summary">Address</label>
	<div class="controls">
		{field:events-address_line_1}<br>
		{field:events-address_line_2}
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="events-summary">City</label>
	<div class="controls">
		{field:events-city}
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="events-summary">ZIP</label>
	<div class="controls">
		{field:events-zip}
	</div>
</div>

<h3>Contact info</h3>

<div class="control-group">
	<label class="control-label" for="events-contact_name">Contact name</label>
	<div class="controls">
		{field:events-contact_name}
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="events-contact_phone">Contact phone</label>
	<div class="controls">
		{field:events-contact_phone}
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="events-contact_email">Contact email</label>
	<div class="controls">
		{field:events-contact_email}
	</div>
</div>

<h3>Ticket info</h3>

<div class="control-group">
	<label class="control-label" for="events-ticket_info">Ticket info</label>
	<div class="controls">
		{field:events-ticket_info}
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="events-ticket_info">Audience</label>
	<div class="controls">
		{field:events-audience}
	</div>
</div>

<h3>Web links</h3>

<div class="control-group">
	<label class="control-label" for="events-website_url">Website URL</label>
	<div class="controls">
		{field:events-website_url}
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="events-facebook_event_url">Facebook event URL</label>
	<div class="controls">
		{field:events-facebook_event_url}
	</div>
</div>


<div class="control-group">
	<div class="controls">
		<input class="btn" type="submit" value="Submit">
	</div>
</div>

{!--


{events-unlisted_venue_type}



{events-imported_row_number}
{events-imported_author_first_nam}
{events-imported_author_last_name}
{events-imported_author_email}
{events-imported_author_phone}
{events-imported_type}
{events-imported_frequency}
{events-imported_date}
{events-imported_time}
{events-imported_end_time}
{events-imported_photo}
{events-imported_series_title}
{events-user_notes}
{events-internal_notes}
{events-internal_tags}


--}

{!--
{custom_fields}
<div class="control-group">
	<label class="control-label" for="{field_name}">{field_label}</label>
	<div class="controls">
		{display_field}
	</div>
</div>
{/custom_fields}
--}