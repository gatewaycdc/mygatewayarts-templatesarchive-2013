<div class="well">

<form method="post" action="{current_url}">

<div class="control-group">
	<label class="control-label" for="medium_categories">Search by Business Type</label>
	<div class="controls">
		<select id="business_categories" name="business_categories">
		{exp:channel:categories style="linear" category_group="2"}
			<option value="{category_id}">{category_name}</option>
		{/exp:channel:categories}
		</select>
	</div>
</div>

<div class="control-group">
	<div class="controls">
		<input class="btn" type="submit" value="go">
	</div>
</div>

</form>

</div>