<div class="control-group">
	<label class="control-label" for="title">Give your listing a unique title. Something catchy!</label>
	<div class="controls">
		<input type="text" name="title">
	</div>
</div>

<h3> Contact Info</h3>

<p class="form_instructions">{instructions:studio_listings-contact_name}</p>
<div class="control-group">
	<label class="control-label" for="studio_listings-contact_name">Contact Name</label>
	<div class="controls">
		{field:studio_listings-contact_name}
	</div>
</div>

<p class="form_instructions">{instructions:studio_listings-contact_phone}</p>
<div class="control-group">
	<label class="control-label" for="studio_listings-contact_phone">Contact Phone</label>
	<div class="controls">
		{field:studio_listings-contact_phone}
	</div>
</div>

<p class="form_instructions">{instructions:studio_listings-contact_email}</p>
<div class="control-group">
	<label class="control-label" for="studio_listings-contact_email">Contact Email</label>
	<div class="controls">
		{field:studio_listings-contact_email}
	</div>
</div>

<h3>Listing Info</h3>

<p class="form_instructions">{instructions:studio_listings-description}</p>
<div class="control-group">
	<label class="control-label" for="studio_listings-description">Description</label>
	<div class="controls">
		{field:studio_listings-description}
	</div>
</div>

<p class="form_instructions">{instructions:studio_listings-sqft}</p>
<div class="control-group">
	<label class="control-label" for="studio_listings-sqft">Sq. Ft.</label>
	<div class="controls">
		{field:studio_listings-sqft}
	</div>
</div>

<h3>Location</h3>

<p class="form_instructions">{instructions:studio_listings-business_listing}</p>
<div class="control-group">
	<label class="control-label" for="studio_listings-business_listing">Is this space part of a Studio, Incubator, or Collaborative that appears in our Directory? Select to link to that business.</label>
	<div class="controls">
		{field:studio_listings-business_listing}
	</div>
</div>

<p class="form_instructions">{instructions:studio_listings-alternate_addres}</p>
<div class="control-group">
	<label class="control-label" for="studio_listings-alternate_addres">If not, what is the address?</label>
	<div class="controls">
		{field:studio_listings-alternate_addres}
	</div>
</div>

<h3>Amenities</h3>

<p class="form_instructions">{instructions:studio_listings-industrial_elect}</p>
<div class="control-group">
	<label class="control-label" for="studio_listings-industrial_elect">Industrial Electrical</label>
	<div class="controls">
		{field:studio_listings-industrial_elect}
	</div>
</div>

<p class="form_instructions">{instructions:studio_listings-ventilation}</p>
<div class="control-group">
	<label class="control-label" for="studio_listings-ventilation">Industrial Ventilation</label>
	<div class="controls">
		{field:studio_listings-ventilation}
	</div>
</div>

<p class="form_instructions">{instructions:studio_listings-hazardous_materi}</p>
<div class="control-group">
	<label class="control-label" for="studio_listings-hazardous_materi">Hazardous materials allowed</label>
	<div class="controls">
		{field:studio_listings-hazardous_materi}
	</div>
</div>

<p class="form_instructions">{instructions:studio_listings-handicap_accessi}</p>
<div class="control-group">
	<label class="control-label" for="studio_listings-handicap_accessi">Handicap-accessible</label>
	<div class="controls">
		{field:studio_listings-handicap_accessi}
	</div>
</div>

<p class="form_instructions">{instructions:studio_listings-restroom}</p>
<div class="control-group">
	<label class="control-label" for="studio_listings-restroom">Restroom</label>
	<div class="controls">
		{field:studio_listings-restroom}
	</div>
</div>

<p class="form_instructions">{instructions:studio_listings-sink}</p>
<div class="control-group">
	<label class="control-label" for="studio_listings-sink">Sink</label>
	<div class="controls">
		{field:studio_listings-sink}
	</div>
</div>

<p class="form_instructions">{instructions:studio_listings-24_hour_access}</p>
<div class="control-group">
	<label class="control-label" for="studio_listings-24_hour_access">24-hour access</label>
	<div class="controls">
		{field:studio_listings-24_hour_access}
	</div>
</div>

<p class="form_instructions">{instructions:studio_listings-24_hour_heating}</p>
<div class="control-group">
	<label class="control-label" for="studio_listings-24_hour_heating">24-hour heating</label>
	<div class="controls">
		{field:studio_listings-24_hour_heating}
	</div>
</div>

<p class="form_instructions">{instructions:studio_listings-24_hour_cooling}</p>
<div class="control-group">
	<label class="control-label" for="studio_listings-24_hour_cooling">24-hour cooling</label>
	<div class="controls">
		{field:studio_listings-24_hour_cooling}
	</div>
</div>

<p class="form_instructions">{instructions:studio_listings-parking}</p>
<div class="control-group">
	<label class="control-label" for="studio_listings-parking">Parking</label>
	<div class="controls">
		{field:studio_listings-parking}
	</div>
</div>

<p class="form_instructions">{instructions:studio_listings-telephone_servic}</p>
<div class="control-group">
	<label class="control-label" for="studio_listings-telephone_servic">Telephone service</label>
	<div class="controls">
		{field:studio_listings-telephone_servic}
	</div>
</div>

<p class="form_instructions">{instructions:studio_listings-internet_service}</p>
<div class="control-group">
	<label class="control-label" for="studio_listings-internet_service">Internet service</label>
	<div class="controls">
		{field:studio_listings-internet_service}
	</div>
</div>



{!--


<div class="control-group">
	<label class="control-label" for=""></label>
	<div class="controls">
		{field:}
	</div>
</div>

--}


<div class="control-group">
	<div class="controls">
		<input class="btn" type="submit" value="Submit">
	</div>
</div>