<div class="well">

<h3>Business Search</h3>

{exp:low_search:form query="{segment_4}" result_page="directory/businesses/search"}
  <fieldset>

    {if error_message != ''}
      <p class="error">{error_message}</p>
    {/if}

    <input type="search" name="keywords" placeholder="Keywords" value="{keywords}" />

    <input type="hidden" name="loose_ends" value="yes">

    Business Categories <select name="category:business_categories[]" multiple="multiple">
      {exp:channel:categories channel="directory" style="linear" category_group="2"}
        <option value="{category_id}"{if category_id IN ({low_search_category})} selected="selected"{/if}>
          {category_name}
        </option>
      {/exp:channel:categories}
    </select>
    
    Media <select name="category:media_categories[]" multiple="multiple">
      {exp:channel:categories channel="directory" style="linear" category_group="1"}
        <option value="{category_id}"{if category_id IN ({low_search_category})} selected="selected"{/if}>
          {category_name}
        </option>
      {/exp:channel:categories}
    </select>

    <button type="submit">Search</button>
  </fieldset>
{/exp:low_search:form}

</div>