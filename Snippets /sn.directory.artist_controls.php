<form method="post" action="{current_url}">

<div class="control-group">
	<label class="control-label" for="medium_categories">Search by Medium</label>
	<div class="controls">
		<select id="medium_categories" name="medium_categories">
		{exp:channel:categories style="linear" category_group="1"}
			<option value="{category_id}">{category_name}</option>
		{/exp:channel:categories}
		</select>
	</div>
</div>

<div class="control-group">
	<div class="controls">
		<input class="btn" type="submit" value="go">
	</div>
</div>

</form>