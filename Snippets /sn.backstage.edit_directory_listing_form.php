<p><em>Both our Artist Directory & Business Directory run from the same database. Fill out our form below to describe how you'd like your directory listing to be seen.
<br>If you're an artist who also owns a business, and you don't want to make a separate directory listing, enter in a different "Trade Name" and check both "Artist" & "Business."</em></p>

<h3>Basic Contact Info</h3>

<div class="control-group">
	<label class="control-label" for="">This listing is for a/an:</label>
	<div class="controls">
		
		<label for="artist_checkbox"><input type="checkbox" name="category[]" id="artist_checkbox" value="76" {embed="directory/.helper-in_cat" entry_id="{entry_id}" cat_id="76" text="checked"}> Artist</label>
		<label for="business_checkbox"><input type="checkbox" name="category[]" id="business_checkbox" value="75" {embed="directory/.helper-in_cat" entry_id="{entry_id}" cat_id="75" text="checked"}> Business</label>
		<label for="venue_checkbox"><input type="checkbox" name="category[]" id="venue_checkbox" value="77" {embed="directory/.helper-in_cat" entry_id="{entry_id}" cat_id="77" text="checked"}> Event Venue</label>			
			
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="title">Name / Listing Title <br><em>(required)</em></label>
	<div class="controls">
		<input type="text" name="title" value="{title}">
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="">E-mail</label>
	<div class="controls">
		{field:directory-email}
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="">Phone</label>
	<div class="controls">
		{field:directory-phone}
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="">Website URL, include http://</label>
	<div class="controls">
		{field:directory-website_url}
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="">Facebook URL, include http://</label>
	<div class="controls">
		{field:directory-facebook_url}
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="">Twitter handle</label>
	<div class="controls">
		<div class="input-prepend">
			<span class="add-on">@</span><input class="span2" type="text" value="{directory-twitter_handle}">
		</div>
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="">LinkedIn URL, include http://</label>
	<div class="controls">
		{field:directory-linkedin_url}
	</div>
</div>

<h3>Location Info</h3>

<div class="control-group">
	<label class="control-label" for="">Street Address</label>
	<div class="controls">
		{field:directory-street_address}
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="">Address Description / Second Line</label>
	<div class="controls">
		{field:directory-address_description}
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="">City</label>
	<div class="controls">
		{field:directory-city}
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="">ZIP code</label>
	<div class="controls">
		{field:directory-zip}
	</div>
</div>

<h3>Map Location</h3>

<p>
<label for="">If located within the Arts District, enter in your address to appear on our map.</label>
</p>
	
{field:directory-map_location}

<h3>Describe Your Work</h3>

<div class="control-group">
	<label class="control-label" for="">About / Biography<br><br><i>Select 'Source' to copy+paste HTML, to further customize your biography, or embed videos.</i></label>
	<div class="controls">
		{field:directory-about}
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="">Artistic / Mission Statement</label>
	<div class="controls">
		{field:directory-artist_statement}
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="directory-trade_name">Media</label>
	<div class="controls">
		
	<select multiple name="category[]">
	{exp:gwcode_categories group_id="1" class="block_list" variable_prefix=""}
	
	<option value="{cat_id}" {embed="directory/.helper-in_cat" entry_id="{entry_id}" cat_id="{cat_id}" text="selected"}>
	{cat_name}
	</option>
	
	{/exp:gwcode_categories}
	</select>
	<p><em>(Hold <strong>Ctrl</strong> [on PC] or <strong>Cmd</strong> [on Mac] to select/un-select multiple categories.)</em></p>
	
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="">Portfolio Uploads<br><br><i>(Add up to 10 images, select 'Upload Image')</i></label>
	<div class="controls">
		{field:directory-images}
	</div>
</div>

<h3>Business or Studio Info</h3>
If you own a business or studio, enter in your information below. Otherwise, just scroll down and click 'Submit.'
<div class="control-group">
	<label class="control-label" for="directory-trade_name">Business/Trade Name<br><em>(Will appear as written.)</em></label>
	<div class="controls">
		{field:directory-trade_name}
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="directory-trade_name">Category</label>
	<div class="controls">

	<select name="category[]" multiple="multiple">
	{exp:gwcode_categories group_id="2" class="block_list" variable_prefix=""}
	
	<option value="{cat_id}" {embed="directory/.helper-in_cat" entry_id="{entry_id}" cat_id="{cat_id}" text="selected"}>
	{cat_name}
	</option>
	
	{/exp:gwcode_categories}
	</select>
	<p><em>(Hold <strong>Ctrl</strong> [on PC] or <strong>Cmd</strong> [on Mac] to select/un-select multiple categories.)</em></p>
	
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="">Handicap Accessible</label>
	<div class="controls">
		{field:directory-business_handicap_acce}
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="">501(c)(3) Non-profit</label>
	<div class="controls">
		{field:directory-business_501c3}
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="">Looking for studio space?</label>
	<div class="controls">
		{field:directory-looking_for_studio_spa}
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="">Taxpayer ID / EID (for internal use only)</label>
	<div class="controls">
		{field:directory-business_taxpayer_id_e}
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="">Do you host regular events?</label>
	<div class="controls">
		{field:directory-business_holds_regular}
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="">Do you keep regular hours of operation?</label>
	<div class="controls">
		{field:directory-business_has_regular_h}
	</div>
</div>

<h3>Hours of Operation</h3>

<p><em>Select <strong>+</strong> to add hours for each day [or group of days].</em></p>
		{field:directory-business_hours}
		

{!--

<div class="control-group">
	<label class="control-label" for="">Facebook URL</label>
	<div class="controls">
		{field:directory-}
	</div>
</div>

{directory-trade_name}
{directory-email}
{directory-phone}
{directory-website_url}
{directory-facebook_url}
{directory-twitter_handle}
{directory-linkedin_url}
{directory-reviews_url}

{directory-business_handicap_acce}
{directory-business_has_regular_h}CLICK TO COPY
{directory-business_holds_regular}
{directory-business_hours}
{directory-business_taxpayer_id_e}
{directory-business_501c3}

{directory-about}
{directory-artist_statement}
{directory-images}

{directory-street_address}
{directory-address_description}
{directory-city}
{directory-zip}
{directory-map_location}

{directory-user_notes}
{directory-internal_notes}
{directory-import_tags}
{directory-import_row_number}
{directory-import_first_name}
{directory-import_last_name}
{directory-import_phone}
{directory-import_phone_2}
{directory-import_alt_zip}
{directory-import_email}
{directory-import_sun_open}
{directory-import_sun_close}
{directory-import_mon_open}
{directory-import_mon_close}
{directory-import_tue_open}
{directory-import_tue_close}
{directory-import_wed_open}
{directory-import_wed_close}
{directory-import_thu_open}
{directory-import_thu_close}
{directory-import_fri_open}
{directory-import_fri_close}
{directory-import_sat_open}
{directory-import_sat_close}
{directory-import_business_type}
{directory-import_medium}


<div class="control-group">
	<label class="control-label" for="">Facebook URL</label>
	<div class="controls">
		{field:directory-}
	</div>
</div>

--}


{!--
{custom_fields}
<div class="control-group">
	<label class="control-label" for="{field_name}">{field_label}</label>
	<div class="controls">
		{display_field}
	</div>
</div>
{/custom_fields}
--}



<div class="control-group">
	<div class="controls">
		<input class="btn" type="submit" value="Submit">
	</div>
</div>