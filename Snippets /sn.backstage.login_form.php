{exp:zoo_visitor:login_form return="/backstage" error_handling="inline" class="form-horizontal"}

{if error:login}
<div class="alert alert-error">{error:login}</div>
{/if}

<div class="control-group">
	<label class="control-label" for="username">Email</label>
	<div class="controls">
		<input type="text" name="username" id="username" class="input-xlarge">
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="password">Password</label>
	<div class="controls">
		<input type="password" name="password" id="password" class="input-xlarge">
	</div>
</div>

{if auto_login}
<div class="control-group">
	<label class="control-label" for="auto_login">Remember me?</label>
	<div class="controls">
		<input class="checkbox" name="auto_login" type="checkbox" value="1" />
	</div>
</div>
{/if}

<div class="control-group">
	<div class="controls">
		<input name="submit" type="submit" value="Log in" />
	</div>
</div>

<p>Don't have a MyGatewayArts account yet? <a href="/backstage/register">Register here!</a></p>
<p>Forgot your password? E-mail our <a href="mailto:gatewayartsdistrict@mygatewayarts.org">Content Management Team</a></p>
{/exp:zoo_visitor:login_form}