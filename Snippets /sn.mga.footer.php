<div class="footer">
<div class="g_c">

<div class="g_12">

<p>

<div class="footer_social_links">
	<a href="http://www.twitter.com/mygatewayarts" class="ir social_icon twitter_icon">Twitter</a>
	<a href="http://www.facebook.com/mygatewayarts" class="ir social_icon facebook_icon">Facebook</a>
</div>

{exp:low_variables:pair var="lv_footer_links" backspace="3"}{if mga_page}<a href="{mga_page}">{link_text}</a>{if:elseif external_url}<a href="{external_url}">{link_text}</a>{if:else}{link_text}{/if} / {/exp:low_variables:pair}

</p>
	
</div>

</div><!-- / .g_c -->
</div><!-- / .footer -->