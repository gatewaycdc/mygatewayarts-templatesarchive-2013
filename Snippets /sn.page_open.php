<!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]> <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]> <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->

	<head>
		<meta charset="utf-8">
		<title>{if page_title}{page_title} | {/if}MyGatewayArts</title>
		<link rel="stylesheet" href="{path=styles/master}">
		<script type="text/javascript" src="{path=scripts/lib-jquery-171-min}"> </script>
		<link rel="shortcut icon" type="image/x-icon" href="/favicon.ico">
	</head>
	
	<body class="p_{structure:page:slug}{if segment_1} seg1_{segment_1}{/if}{if segment_2} seg2_{segment_2}{/if}{if segment_3} seg3_{segment_3}{/if} {page_color_context}" id="{structure:page:slug}">
	
	<div class="page_wrapper">