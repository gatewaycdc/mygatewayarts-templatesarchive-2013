<!-- template debug note - begin masthead snippet in sn.masthead - {current_time format="%Y-%m-%d %H:%i:%s"} - member {member_id} -->

<div class="masthead">
<div class="g_c">
	
	<div class="masthead_login">
		{if logged_out}<a class="login_link" href="/backstage">Log In</a> | <a href="/backstage/register">Register</a> | <a class="subscribe_link" href="/play/news-about-the-arts-district/join-our-mailing-list/">Mailing List</a>{/if}
		{if logged_in}Welcome back{if screen_name}, {screen_name}{/if} | <a href="/backstage">Dashboard</a> | <a class="login_link" href="{path=logout}">Log Out</a>{/if}
	</div>
	
	<div class="masthead_search">
		{exp:low_search:form class="form-search" collection="{sn.search.site_search_collections}" search_mode="all" result_page="search"}
		<fieldset>
			<p>
			<input type="search" name="keywords" class="input-medium search-query" placeholder="Search...">
			</p>
		</fieldset>
		{/exp:low_search:form}
	</div>
	
	<h1><a class="ir logo_mark" href="/">Gateway Arts District</a></h1>
	
	<div class="masthead_social_links">
		<a href="http://www.twitter.com/mygatewayarts" class="ir social_icon twitter_icon">Twitter</a>
		<a href="http://www.facebook.com/mygatewayarts" class="ir social_icon facebook_icon">Facebook</a>
	</div>

</div><!-- / .g_c -->
</div><!-- / .masthead -->